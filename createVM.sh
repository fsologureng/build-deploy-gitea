#!/bin/bash -ex

NAME=$1
DOMAIN=codeberg-testing.org

test -x authorized_keys || cp -v ${HOME}/.ssh/authorized_keys .

ssh-keygen -R ${NAME}

virsh undefine --remove-all-storage ${NAME} || true

virt-install -v \
	--virt-type kvm \
	--name ${NAME} \
	--initrd-inject preseed.cfg \
	--initrd-inject authorized_keys \
	--location http://http.debian.net/debian/dists/stable/main/installer-amd64/ \
	--extra-args "auto console=ttyS0 hostname=${NAME} domain=${DOMAIN}" \
	--ram 8192 \
	--vcpus 4 \
	--disk size=300 \
	--memory 1024 \
	--graphics none \
	--network bridge:lxcbr0,model=virtio \
	--os-variant debian10

