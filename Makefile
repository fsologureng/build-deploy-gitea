
HOSTNAME_FQDN := codeberg-test.org
GO_VERSION ?= 1.18.5

export PATCHDIR := ${PWD}/local_patches
export BUILDDIR := ${PWD}/build
export GITEASRC := ${BUILDDIR}/gitea
export GOROOT := ${BUILDDIR}/go-${GO_VERSION}/go
export GOPATH := ${BUILDDIR}/gitea-bin
export PATH := ${GOROOT}/bin:${GOPATH}/bin:${PATH}

GOTAR := go${GO_VERSION}.$(shell uname | tr [:upper:] [:lower:])-amd64.tar.gz
ORIGIN = https://codeberg.org/Codeberg/gitea
GITEA_BRANCH := codeberg-1.17

IMAGE_PREFIX = etc/gitea/public/img
IMAGES = \
	${IMAGE_PREFIX}/logo.svg \
	${IMAGE_PREFIX}/logo.png \
	${IMAGE_PREFIX}/logo-small.svg \
	${IMAGE_PREFIX}/logo-medium.svg \
	${IMAGE_PREFIX}/favicon.ico \
	${IMAGE_PREFIX}/favicon.png \
	${IMAGE_PREFIX}/favicon.svg \
	${IMAGE_PREFIX}/codeberg.png \
	${IMAGE_PREFIX}/gitea-safari.svg

MATHJAX = etc/gitea/public/mathjax
THREEJS = etc/gitea/public/three.js

TARGETS = \
	${IMAGES} \
	${MATHJAX} \
	${THREEJS} \
	${GOPATH}/bin/gitea

all : ${TARGETS}
images : ${IMAGES}

${GOPATH}/bin/gitea : ${GITEASRC}
	TAGS="bindata" make -j1 -C $< build install

${GITEASRC} : ${GOROOT}/bin/go
	( mkdir -p $@ && cd $@ && git clone --depth 100 ${ORIGIN} . -b ${GITEA_BRANCH} )
	( cd $@ && [ -d "${PATCHDIR}" ] && cat $(wildcard ${PATCHDIR}/*.diff) | patch -p1 )
	( cd $@ && cp -v contrib/options/label/Advanced options/label/Advanced )
	( cd $@ && find . -type f -print0 | xargs -0 sed -i 's/\/sign_up/\/sing_up/g' )

${GOROOT}/bin/go :
	-rm -r ${BUILDDIR}/go-*
	mkdir -p ${GOROOT}/Downloads
	wget -c --no-verbose --directory-prefix=${GOROOT}/Downloads https://dl.google.com/go/${GOTAR}
	tar xfz ${GOROOT}/Downloads/${GOTAR} -C ${BUILDDIR}/go-${GO_VERSION}
	rm -r ${GOROOT}/Downloads

deployment : deploy-gitea

deploy-gitea : ${GOPATH}/bin/gitea ${TARGETS}
	ssh root@${HOSTNAME_FQDN} mkdir -p /data/git/bin
	scp $< root@${HOSTNAME_FQDN}:/data/git/bin/gitea.new
	./deployConfigFiles.sh ${HOSTNAME_FQDN}

${IMAGE_PREFIX}/logo.svg : codeberg.svg
	mkdir -p $(dir $@)
	cp -v $< $@
	! which svgo || svgo -i $@ -o $@ --multipass

${IMAGE_PREFIX}/logo-medium.svg : codeberg-favicon.svg
	mkdir -p $(dir $@)
	cp -v $< $@
	! which svgo || svgo -i $@ -o $@ --multipass

${IMAGE_PREFIX}/logo-small.svg : codeberg-white.svg
	mkdir -p $(dir $@)
	cp -v $< $@
	! which svgo || svgo -i $@ -o $@ --multipass
	
${IMAGE_PREFIX}/favicon.svg : codeberg-favicon.svg
	mkdir -p $(dir $@)
	cp -v $< $@
	! which svgo || svgo -i $@ -o $@ --multipass

${IMAGE_PREFIX}/favicon.png : codeberg-favicon.svg
	rsvg-convert -h 64 -o $@ $<

${IMAGE_PREFIX}/favicon.ico : ${IMAGE_PREFIX}/favicon.png
	convert -background none $< -define icon:auto-resize=64,48,32,24,16 $@

${IMAGE_PREFIX}/codeberg.png : ${IMAGE_PREFIX}/logo-medium.svg
	rsvg-convert -h 290 -o $@ $<
	
${IMAGE_PREFIX}/logo.png : ${IMAGE_PREFIX}/logo-medium.svg
	rsvg-convert -w 256 -o $@ $<

${IMAGE_PREFIX}/gitea-safari.svg : ${IMAGE_PREFIX}/logo-medium.svg
	cp -v $< $@

${MATHJAX} :
	echo "*** SKIPPING MATHJAX INTEGRATION, NOT READY FOR PRODUCTION YET"
	##mkdir -p ${BUILDDIR}
	##git clone --depth=1 https://github.com/mathjax/MathJax.git ${BUILDDIR}/MathJax.git
	##mv -v ${BUILDDIR}/MathJax.git/es5 $@
	##rm -rf ${BUILDDIR}/MathJax.git

${THREEJS} :
	mkdir -p ${BUILDDIR}
	mkdir -p $@/examples/jsm
	git clone --depth=1 https://github.com/mrdoob/three.js.git ${BUILDDIR}/three.js.git
	mv -v ${BUILDDIR}/three.js.git/build $@/
	mv -v ${BUILDDIR}/three.js.git/examples/jsm/controls $@/examples/jsm/
	mv -v ${BUILDDIR}/three.js.git/examples/jsm/loaders $@/examples/jsm/
	rm -rf ${BUILDDIR}/three.js.git

clean :
	rm -f ${BUILDDIR}/gitea-bin/bin/gitea
	rm -rf ${GITEASRC}

realclean :
	rm -rf ${TARGETS}
	rm -rf ${BUILDDIR}

withdocker:
	mkdir -p ${BUILDDIR}
	docker build -t codeberg.org/codeberg/build-deploy-gitea .
	docker run -it --rm -v "$(shell pwd):/build" -w /build -e "TARGET_UID=$(shell id -u)" codeberg.org/codeberg/build-deploy-gitea make

withdocker-deploy:
	mkdir -p ${BUILDDIR}
	docker run -it --rm -v "$(shell pwd):/build" -v "$$HOME/.ssh:/root/.ssh:ro" -e "TARGET_UID=$(shell id -u)" -w /build codeberg.org/codeberg/build-deploy-gitea make deployment
